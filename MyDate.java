package btt2;

import java.util.Scanner;

public class MyDate {

    private int date;
    private int month;
    private int year;

    public MyDate() {
        this.date = 1;
        this.month = 1;
        this.year = 1950;
    }

    public MyDate(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }

    public void setDate() {
        Scanner doc = new Scanner(System.in);
        System.out.println("Nhập ngày: ");
        this.date = Integer.parseInt(doc.nextLine());
        System.out.println("Nhập tháng: ");
        this.month = Integer.parseInt(doc.nextLine());
        System.out.println("Nhập năm: ");
        this.year = Integer.parseInt(doc.nextLine());
    }

    public void printDate() {
        System.out.println(this.date + "/" + this.month + "/" + this.year);
    }

}
