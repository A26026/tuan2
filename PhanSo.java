
package btt2;


public class PhanSo {
    public final int tu;
    public final int mau;
    public PhanSo()
    {
        this.tu = 0;
        this.mau = 0;
    }
    public PhanSo(int tu,int mau)
    {
        this.tu = tu;
        this.mau = mau;
    }
    public PhanSo cong(PhanSo a)
    {
        return new PhanSo(this.tu*a.mau + a.tu*this.mau,this.mau * a.mau);
    }
    public PhanSo tru(PhanSo a)
    {
        return new PhanSo(this.tu*a.mau - a.tu*this.mau,this.mau * a.mau);
    }
     public PhanSo nhan(PhanSo a)
    {
        return new PhanSo(this.tu*a.tu,this.mau * a.mau);
    }
      public PhanSo chia(PhanSo a)//b/a
    {
          
        return new PhanSo(this.tu*a.mau,this.mau * a.tu);
    }
      public void print()
      {
          System.out.println(this.tu+"/"+this.mau);
      }
}
